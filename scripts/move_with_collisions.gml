///move_with_collisions(spd,bounce);
var spd = argument0;
var bounce = argument1;

//Horizontal collision checking
if(place_meeting(x+spd[h],y,obj_solid_parent))
{
    while(place_meeting(x+sign(spd[h]),y,obj_solid_parent))
        x += lengthdir_x(spd[h],dir);
    if(bounce > 0)
        spd[@ h] = -spd[h] * bounce;
    else
        spd[@ h] = 0;
}
else
{
    x += lengthdir_x(spd[h],dir);
}

//Vertical collision checking
if(place_meeting(x,y+spd[v],obj_solid_parent))
{
    while(place_meeting(x,y+sign(spd[v]),obj_solid_parent))
        y += lengthdir_y(spd[v],dir);
    if(bounce > 0)
        spd[@ v] = -spd[v] * bounce;
    else
        spd[@ v] = 0;
}
else
{
    y += lengthdir_y(spd[v],dir);
}
