///save_overwrite(file_name)
///@desc Overwrites a save file with blank data
///@param file_name
var file_name = argument0;

if(!file_exists(file_name))
{
    show_debug_message("Save not found");
    exit;
}
	
//Destroy whatever data exists
ds_map_destroy(global.SAVE_DATA);
global.SAVE_DATA = ds_map_create();

//Writes in default values
ds_map_replace(global.SAVE_DATA,"Name","");

//Save
ds_map_secure_save(global.SAVE_DATA,file_name);
